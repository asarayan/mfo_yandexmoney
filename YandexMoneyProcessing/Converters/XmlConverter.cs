﻿using System.Text;
using System.Xml;
using Newtonsoft.Json;
using YandexMoneyProcessing.Classes;
using YandexMoneyProcessing.Models;
using YandexMoneyProcessing.Services;

namespace YandexMoneyProcessing.Converters
{
    public static class XmlConverter
    {
        public static string ConvertToXml(this DepositionModel depositionModel)
        {
            var xml = new StringBuilder();
            xml.Append($"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            xml.Append($"<{depositionModel.Type}DepositionRequest " +
                       $"agentId=\"{depositionModel.AgentId}\" " +
                       $"clientOrderId=\"{depositionModel.ClientOrderId}\" " +
                       $"requestDT=\"{depositionModel.RequestDt}\" " +
                       $"dstAccount=\"{depositionModel.DstAccount}\" " +
                       $"amount=\"{depositionModel.Amount}\" " +
                       $"currency=\"{depositionModel.Currency}\" " +
                       $"contract=\"{depositionModel.Contract}\"> ");

            xml.Append("<paymentParams>");
            xml.Append(
                $"<skr_orderN>{depositionModel.PaymentParamsModel.SkrOrderN}</skr_orderN>");
            xml.Append($"<pdr_firstName>{depositionModel.PaymentParamsModel.FirstName}</pdr_firstName>" +
                       $"<pdr_middleName>{depositionModel.PaymentParamsModel.MiddleName}</pdr_middleName>" +
                       $"<pdr_lastName>{depositionModel.PaymentParamsModel.LastName}</pdr_lastName>" +
                       $"<pdr_docNumber>{depositionModel.PaymentParamsModel.DocNumber}</pdr_docNumber>" +
                       $"<pdr_postcode>{depositionModel.PaymentParamsModel.PostCode}</pdr_postcode>" +
                       $"<pdr_country>{depositionModel.PaymentParamsModel.Country}</pdr_country>" +
                       $"<pdr_city>{depositionModel.PaymentParamsModel.City}</pdr_city>" +
                       $"<pdr_address>{depositionModel.PaymentParamsModel.Address}</pdr_address>" +
                       $"<pdr_birthDate>{depositionModel.PaymentParamsModel.BirthDate}</pdr_birthDate>" +
                       $"<pdr_birthPlace>{depositionModel.PaymentParamsModel.BirthPlace}</pdr_birthPlace>" +
                       $"<pdr_docIssueYear>{depositionModel.PaymentParamsModel.DocIssueYear}</pdr_docIssueYear>" +
                       $"<pdr_docIssueMonth>{depositionModel.PaymentParamsModel.DocIssueMonth}</pdr_docIssueMonth>" +
                       $"<pdr_docIssueDay>{depositionModel.PaymentParamsModel.DocIssueDay}</pdr_docIssueDay>" +
                       $"<pdr_docIssuedBy>{depositionModel.PaymentParamsModel.DocIssuedBy}</pdr_docIssuedBy>" +
                       $"<pof_offerAccepted>{depositionModel.PaymentParamsModel.OfferAccepted}</pof_offerAccepted>" +
                       $"<smsPhoneNumber>{depositionModel.PaymentParamsModel.SmsPhoneNumber}</smsPhoneNumber>");
            xml.Append($"</paymentParams></{depositionModel.Type}DepositionRequest>");

            return xml.ToString();
        }

        public static YandexResponse XmlToResponse(this byte[] source)
        {
            var str = Encoding.UTF8.GetString(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(str);
            return new YandexResponse()
            {
                Status = doc.DocumentElement?.Attributes["status"]?.InnerText,
                ClientOrderId = doc.DocumentElement?.Attributes["clientOrderId"]?.InnerText,
                ProcessedDT = doc.DocumentElement?.Attributes["processedDT"]?.InnerText,
                Error = doc.DocumentElement?.Attributes["error"]?.InnerText
            };
        }
        public static AuthorizeResponse XmlToAuthorizeResponse(this byte[] source)
        {
            var str = Encoding.UTF8.GetString(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(str);
            return new AuthorizeResponse()
            {
                OrderId = doc.DocumentElement?.GetElementsByTagName("order_id")[0]?.InnerText,
                ContractId = doc.DocumentElement?.GetElementsByTagName("merchant_order_id")[0]?.InnerText,
                Status = doc.DocumentElement?.GetElementsByTagName("status")[0]?.InnerText,
                ResponseCode = doc.DocumentElement?.GetElementsByTagName("responsecode")[0]?.InnerText,
                ResponseCodeText = doc.DocumentElement?.GetElementsByTagName("responsecodetext")[0]?.InnerText,
                Rrn = doc.DocumentElement?.GetElementsByTagName("rrn")[0]?.InnerText,
                Authcode = doc.DocumentElement?.GetElementsByTagName("authcode")[0]?.InnerText,
                UserToken = doc.DocumentElement?.GetElementsByTagName("user_token")[0]?.InnerText,
                Time = doc.DocumentElement?.GetElementsByTagName("time")[0]?.InnerText,
                Eci = doc.DocumentElement?.GetElementsByTagName("eci")[0]?.InnerText,
                MpiResult = doc.DocumentElement?.GetElementsByTagName("mpi_result")[0]?.InnerText,
                Amount = doc.DocumentElement?.GetElementsByTagName("amount")[0]?.InnerText,
                Currency = doc.DocumentElement?.GetElementsByTagName("currency")[0]?.InnerText
            };
        }
        
        public static ErrorReason XmlToErrorReason(this string str)
        {
            var parentDoc = new XmlDocument();
            parentDoc.LoadXml(str);
            var doc = new XmlDocument();
            if (parentDoc.DocumentElement?.InnerXml != null)
                doc.LoadXml(parentDoc.DocumentElement?.InnerXml);

            return new ErrorReason()
            {
                OrderN = doc.DocumentElement?.GetElementsByTagName("orderN")[0]?.InnerText,
                Reason = doc.DocumentElement?.GetElementsByTagName("reason")[0]?.InnerText,
                Amount = doc.DocumentElement?.GetElementsByTagName("amount")[0]?.InnerText,
                Currency = doc.DocumentElement?.GetElementsByTagName("currency")[0]?.InnerText,
                Card = new CardModel
                {
                    BankName = doc.DocumentElement?.GetElementsByTagName("bankName")[0]?.InnerText,
                    Type = doc.DocumentElement?.GetElementsByTagName("type")[0]?.InnerText,
                    ProductCode = doc.DocumentElement?.GetElementsByTagName("productCode")[0]?.InnerText,
                    ProductName = doc.DocumentElement?.GetElementsByTagName("productName")[0]?.InnerText,
                    CountryCode = doc.DocumentElement?.GetElementsByTagName("countryCode")[0]?.InnerText,
                    CountryName = doc.DocumentElement?.GetElementsByTagName("countryName")[0]?.InnerText,
                }
            };
        }

        public static UserToken XmlToUserToken(this string str)
        {
            var doc = new XmlDocument();
            doc.LoadXml(str);

            return new UserToken()
            {
                OrderId = doc.DocumentElement?.GetElementsByTagName("order_id")[0]?.InnerText,
                Usertoken = doc.DocumentElement?.GetElementsByTagName("user_token")[0]?.InnerText,
                ClientCardNumber = doc.DocumentElement?.GetElementsByTagName("client_card_number")[0]?.InnerText,
                ClientCardType = doc.DocumentElement?.GetElementsByTagName("client_card_type")[0]?.InnerText,
            };
        }

        public static bool TryParseXml(this string xml)
        {
            try
            {
                var doc = new XmlDocument();
                doc.LoadXml(xml);
                return true;
            }
            catch (XmlException e)
            {
                return false;
            }
        }
    }
}
