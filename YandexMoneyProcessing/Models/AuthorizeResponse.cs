namespace YandexMoneyProcessing.Models
{
    public class AuthorizeResponse
    {
        public string OrderId { get; set; }
        public string UserToken { get; set; }
        public string ContractId { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public string Time { get; set; }
        public string Comment { get; set; }
        public string Authcode { get; set; }
        public string Eci { get; set; }
        public string MpiResult { get; set; }
        public string Rrn { get; set; }
        public string Status { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseCodeText { get; set; }
    }
}