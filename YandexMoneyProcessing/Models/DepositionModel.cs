﻿namespace YandexMoneyProcessing.Models
{
    public class DepositionModel
    {
        public string AgentId { get; set; }
        public string ClientOrderId { get; set; }
        public string RequestDt { get; set; }
        public string DstAccount { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public string Contract { get; set; }
        public PaymentParamsModel PaymentParamsModel { get; set; }
        public string Type { get; set; }
    }
}
