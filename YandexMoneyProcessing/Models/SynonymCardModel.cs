﻿namespace YandexMoneyProcessing.Models
{
    public class SynonymCardModel
    {
        public string skr_destinationCardCountryCode { get; set; }
        public string skr_destinationCardPanmask { get; set; }
        public string skr_destinationCardSynonim { get; set; }
        public string skr_destinationCardType { get; set; }
        public string reason { get; set; }
    }
}