namespace YandexMoneyProcessing.Models
{
    public class PaymentParamsModel
    {
        public string SkrOrderN { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DocNumber { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string DocIssueYear { get; set; }
        public string DocIssueMonth { get; set; }
        public string DocIssueDay { get; set; }
        public string DocIssuedBy { get; set; }
        public string OfferAccepted { get; set; }
        public string SmsPhoneNumber { get; set; }
    }
}