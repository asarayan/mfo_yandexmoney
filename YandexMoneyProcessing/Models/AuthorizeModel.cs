﻿namespace YandexMoneyProcessing.Models
{
    public class AuthorizeModel
    {
        public string ContractId { get; set; }
        public string UserToken { get; set; }
        public string Amount { get; set; }
        public PaymentParams PaymentParams { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
    }
}