﻿namespace YandexMoneyProcessing.Models
{
    public class ErrorReason
    {
        public string OrderN { get; set; }
        public string Reason { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public CardModel Card { get; set; }
    }
}