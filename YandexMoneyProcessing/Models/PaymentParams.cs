﻿namespace YandexMoneyProcessing.Models
{
    public class PaymentParams
    {
        public string ShopId { get; set; }
        public string ShopArticleId { get; set; }
    }
}