﻿namespace YandexMoneyProcessing.Models
{
    public class UserToken
    {
        public string OrderId { get; set; }
        public string Usertoken { get; set; }
        public string ClientCardNumber { get; set; }
        public string ClientCardType { get; set; }
    }
}