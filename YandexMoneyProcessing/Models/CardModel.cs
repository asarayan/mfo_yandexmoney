﻿namespace YandexMoneyProcessing.Models
{
    public class CardModel
    {
        public string BankName { get; set; }
        public string Type { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}