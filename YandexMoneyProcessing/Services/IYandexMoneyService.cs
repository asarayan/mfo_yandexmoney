﻿using System;
using YandexMoneyProcessing.Classes;
using YandexMoneyProcessing.Models;

namespace YandexMoneyProcessing.Services
{
    public interface IYandexMoneyService
    {
        ErrorReason GetErrorReasonBankCard(string orderN);
        UserToken GetUserTokenBankCard(string orderN);
        YandexResponse TestDepositionToBankCard(DepositionModel depositionModel);
        YandexResponse MakeDepositionToBankCard(DepositionModel depositionModel);
        AuthorizeResponse AuthorizeFromBankCard(AuthorizeModel authorizeModel);
    }
}
