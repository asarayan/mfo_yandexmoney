﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Crypto.Generators;
using YandexMoneyListener.Classes;
using YandexMoneyProcessing.Classes;
using YandexMoneyProcessing.Converters;
using YandexMoneyProcessing.Models;
//using System.Security.Cryptography.Pkcs;

namespace YandexMoneyProcessing.Services
{
    public class YandexMoneyService : IYandexMoneyService
    {
        readonly CommonProtocolUtils _commonProtocolUtils;
        private static readonly string UrlToken = "https://paymentcard.yamoney.ru:9094";
        private static readonly string UrlErrorReason = "https://paymentcard.yamoney.ru";
        private static readonly string UrlDepostion = " https://calypso.yamoney.ru:9094";

        public YandexMoneyService()
        {
            _commonProtocolUtils = new CommonProtocolUtils();
        }

        public ErrorReason GetErrorReasonBankCard(string orderN)
        {
            if (orderN == null)
                return null;

            var url = $"{UrlErrorReason}/webservice/order/api/getErrorReason?orderN={orderN}&skr_checkCard=true";

            return PostQuery(null, url).XmlToErrorReason();
        }

        public UserToken GetUserTokenBankCard(string orderN)
        {
            if (orderN == null)
                return null;

            var url = $"{UrlToken}/gates/system/user_token?order_id={orderN}";
            byte[] response = PostQueryWithDecrypting(null, url);
            var str = Encoding.UTF8.GetString(response);

            return str.XmlToUserToken();
        }

        public YandexResponse TestDepositionToBankCard(DepositionModel depositionModel)
        {
            var url = $"{UrlDepostion}/webservice/deposition/api/testDeposition";
            depositionModel.Type = "test";
            return PostQueryWithDecrypting(depositionModel.ConvertToXml(), url).XmlToResponse();
        }

        public YandexResponse MakeDepositionToBankCard(DepositionModel depositionModel)
        {
            var url = $"{UrlDepostion}/webservice/deposition/api/makeDeposition";
            depositionModel.Type = "make";
            return PostQueryWithDecrypting(depositionModel.ConvertToXml(), url).XmlToResponse();
        }

        public string PostQuery(string queryXml, string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = "POST";
            request.ContentType = "application/pkcs7-mime";
            if (!string.IsNullOrEmpty(queryXml))
            {
                using (var stream = request.GetRequestStream())
                {
                    byte[] testRequestBytes = Encoding.UTF8.GetBytes(queryXml);
                    stream.Write(testRequestBytes, 0, testRequestBytes.Length);
                }
            }

            var response = (HttpWebResponse)request.GetResponse();
            var result = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return result;
        }

        public byte[] PostQueryWithDecrypting(string queryXml, string url)
        {
            //var clientCertificate = new X509Certificate2("201131.p12", "650895");
            var clientCertificate = CommonProtocolUtils.GetClientSertificate();
            var serverCertificate  = CommonProtocolUtils.GetServerSertificate();

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ClientCertificates.Add(clientCertificate);
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = "POST";
            request.ContentType = "application/pkcs7-mime";

            if (!string.IsNullOrEmpty(queryXml))
            {
                string testRequest = _commonProtocolUtils.Sign(Encoding.UTF8.GetBytes(queryXml), _commonProtocolUtils._clientKeys);
                using (var stream = request.GetRequestStream())
                {
                    byte[] testRequestBytes = Encoding.UTF8.GetBytes(testRequest);
                    stream.Write(testRequestBytes, 0, testRequestBytes.Length);
                }
            }

            var response = (HttpWebResponse)request.GetResponse();
            var result = new StreamReader(response.GetResponseStream()).ReadToEnd();

            if (result.TryParseXml())
                return Encoding.UTF8.GetBytes(result);
            var testResponseDocument = _commonProtocolUtils.Verify(result, serverCertificate);//.XmlToResponse();
            return testResponseDocument;
        }

        public SynonymCardModel GetSynonymCard(string cardNumber)
        {
            var url =
                $"{UrlErrorReason}/gates/card/storeCard/?skr_destinationCardNumber={cardNumber}&skr_responseFormat=json";

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            var response = (HttpWebResponse)request.GetResponse();


            if (response.StatusCode != HttpStatusCode.OK) return null;

            var jsonString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            var json =  JObject.Parse(jsonString).SelectToken("storeCard");

            return JsonConvert.DeserializeObject<SynonymCardModel>(json.ToString());
        }

        public static string Test()
        {
            return "4D0F5F28";
        }

        public AuthorizeResponse AuthorizeFromBankCard(AuthorizeModel authorizeModel)
        {
            var url = $"{UrlToken}/gates/system/authorize";
            var getParams = $"amount={authorizeModel.Amount}&user_token={authorizeModel.UserToken}&currency=RUB" +
                            $"&description={authorizeModel.Description}&merchant_order_id={authorizeModel.ContractId}" +
                            $"&payment_params={{\"paymentLinkParams\":{{\"shopId\":{authorizeModel.PaymentParams.ShopId}, " +
                            $"\"shopArticleId\":{authorizeModel.PaymentParams.ShopArticleId}}}}}";
            var response = PostQueryWithDecrypting(null, url+"?"+ getParams);

            return response.XmlToAuthorizeResponse();
        }
    }
}
