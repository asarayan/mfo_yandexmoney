﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace YandexMoneyListener.Classes
{
    public class CertificateWebClient : WebClient
    {
        private readonly X509Certificate2 _certificate;

        public CertificateWebClient(X509Certificate2 cert)
        {
            _certificate = cert;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);

            ServicePointManager.ServerCertificateValidationCallback =
                (obj, X509certificate, chain, errors) => true;

            request.ClientCertificates.Add(_certificate);

            return request;
        }
    }
}
