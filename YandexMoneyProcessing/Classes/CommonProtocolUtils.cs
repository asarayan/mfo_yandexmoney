﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using Org.BouncyCastle.Asn1.Cms;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;

namespace YandexMoneyProcessing.Classes
{
    internal class CommonProtocolUtils
    {
        // ключевая пара клиента
        private const int TcpTimeout = 60000;
        private readonly NumberFormatInfo _amountFormat;
        internal readonly KeyPair _clientKeys;
        // ключевая пара сервера
        internal readonly KeyPair _serverKeys;

        /**
          * Настройки теста
          */
        // TCP timeout, миллисекунд

        // определение временной зоны (UTC)
        private readonly TimeZoneInfo _timeZone;
        // определение формата суммы (0.00)

        internal CommonProtocolUtils()
        {
            // загрузка пар ключей и сертификатов
            _clientKeys = LoadPkcs12();


            // инициализация форматов
            _timeZone = TimeZoneInfo.Utc;
            _amountFormat = new NumberFormatInfo {CurrencyDecimalSeparator = ".", NumberDecimalDigits = 2};
        }

        /// <summary>
        /// Загрузка ключевой пары из файла формата PKCS#12 (OpenSSL)
        /// </summary>
        /// <param name="filename">путь к файлу</param>
        /// <param name="password">пароль к файлу</param>
        /// <returns></returns>
        private KeyPair LoadPkcs12()
        {
            var cer = GetClientSertificate();
            var keyParameter = Org.BouncyCastle.Security.DotNetUtilities.GetKeyPair(cer.PrivateKey).Private;
            var cert = Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(cer);
            return new KeyPair(keyParameter, cert);
        }

        public static X509Certificate2 GetClientSertificate()
        {
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            //X509Certificate2Collection cers = store.Certificates.Find(X509FindType.FindBySerialNumber, "32000045A7EF9CA377E5C8A38E0002000045A7", false);
            X509Certificate2Collection cers = store.Certificates.Find(X509FindType.FindBySerialNumber, "32000045A7EF9CA377E5C8A38E0002000045A7", false);
            if (cers.Count > 0)
            {
                cer = cers[0];
            };
            store.Close();
            return cer;
        }

        /// <summary>
        /// Создание пустого XML документа
        /// </summary>
        /// <returns></returns>
        internal XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);
            return doc;
        }

        /// <summary>
        /// Сериализация XML документа
        /// </summary>
        /// <param name="doc">XML документ</param>
        /// <returns>сериализованные данные</returns>
        internal byte[] Serialize(XmlDocument doc)
        {
            var ms = new MemoryStream();
            XmlWriter w = new XmlTextWriter(ms, Encoding.UTF8);
            doc.WriteTo(w);
            w.Close();
            return ms.ToArray();
        }

        /// <summary>
        /// Десериализация XML документа
        /// </summary>
        /// <param name="data">сериализованные данные</param>
        /// <returns>корневой элемент разобранного документа</returns>
        internal XmlElement Deserialize(byte[] data)
        {
            var doc = new XmlDocument();
            doc.Load(new XmlTextReader(new MemoryStream(data)));
            return doc.DocumentElement;
        }

        /// <summary>
        /// Изготовление PKCS#7 пакета в PEM формате (OpenSSL), содержащего:
        /// 1. пользовательские данные 
        /// 2. подпись, изготовленную ключевой парой пользователя
        /// </summary>
        /// <param name="source">пользовательские данные</param>
        /// <param name="keyPair">ключевая пара для изготовления подписи</param>
        /// <returns>PKCS#7 пакет в сериализованном виде</returns>
        internal string Sign(byte[] source, KeyPair keyPair)
        {
            var gen = new CmsSignedDataGenerator();
            gen.AddSigner(keyPair.PrivateKey, keyPair.Certificate, CmsSignedGenerator.DigestSha1);
            CmsSignedData s = gen.Generate(new CmsProcessableByteArray(source), true);
            var sw = new StringWriter();
            var writer = new PemWriter(sw);
            writer.WriteObject(s.ContentInfo);
            writer.Writer.Close();
            return sw.ToString();
        }

        /// <summary>
        /// Распаковка PKCS#7 пакета в PEM формате (OpenSSL) и проверка подписи согласно требуемому сертификату
        /// </summary>
        /// <param name="data">PKCS#7 пакет в сериализованном виде</param>
        /// <param name="certificate">X509 сертификат, по которому сверяется подпись</param>
        /// <returns>пользовательские данные, извлеченные из пакета</returns>
        internal byte[] Verify(string data, X509Certificate certificate)
        {
            var reader = new PemReader(new StringReader(data));
            var signedData = new CmsSignedData((ContentInfo) reader.ReadObject());
            return (byte[])signedData.SignedContent.GetContent();
        }

        public static X509Certificate GetServerSertificate()
        {
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cers = store.Certificates.Find(X509FindType.FindBySerialNumber, "4D5A2566", false);
            if (cers.Count > 0)
            {
                cer = cers[0];
            };
            store.Close();
            return Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(cer);
        }
    }

    internal class KeyPair
    {
        /// <summary>
        /// Сертификат (публичная часть ключевой пары)
        /// </summary>
        private readonly X509Certificate _certificate;

        /// <summary>
        /// Секретная часть ключевой пары
        /// </summary>
        private readonly AsymmetricKeyParameter _privateKey;

        public KeyPair(AsymmetricKeyParameter privateKey, X509Certificate certificate)
        {
            _privateKey = privateKey;
            _certificate = certificate;
        }

        public AsymmetricKeyParameter PrivateKey
        {
            get { return _privateKey; }
        }

        public X509Certificate Certificate
        {
            get { return _certificate; }
        }
    }
}