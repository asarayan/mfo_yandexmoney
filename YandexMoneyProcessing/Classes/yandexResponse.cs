﻿namespace YandexMoneyProcessing.Classes
{
    public class YandexResponse
    {
        string Code { get; set; }
        string Message { get; set; }
        public string Status { get; set; }
        public string ClientOrderId { get; set; }
        public string ProcessedDT { get; set; }
        public string Error { get; set; }

    }
}
