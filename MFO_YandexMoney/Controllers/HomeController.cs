﻿using System;
using System.Web.Mvc;
using YandexMoneyProcessing.Models;
using YandexMoneyProcessing.Services;

namespace MFO_YandexMoney.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string reason, string merchant_order_id, string cps_card, string orderN2)
        {
            var orderN = "tX6Ayzt5pGqnlQBRAyLInmrYXoEZ.001f.201607";
            var service = new YandexMoneyService();
           
            var errorReason = service.GetErrorReasonBankCard(orderN);
            var token = service.GetUserTokenBankCard(orderN);
            var deposionModel = GetFakeDepostionModel(orderN);
            var testDepositionResult = service.TestDepositionToBankCard(deposionModel);
            if (testDepositionResult.Status == "0")
            {
                var makeDepositionResult = service.MakeDepositionToBankCard(deposionModel);
            }
            return View();
        }

        private DepositionModel GetFakeDepostionModel(string orderN)
        {
            Random rnd = new Random();
            var depositionModel = new DepositionModel
            {
                AgentId = "201131",
                DstAccount = "257003392579",
                ClientOrderId = rnd.Next(10, 10000000).ToString(),
                RequestDt = DateTime.Now.ToString("o"),
                Amount = "100.00",
                Currency = "10643",
                Contract = "Потому что заслужил",
                PaymentParamsModel = new PaymentParamsModel
                {
                    SkrOrderN = orderN,
                    FirstName = "Аркадий",
                    MiddleName = "Михайлович",
                    LastName = "Сараян",
                    DocNumber = "4002109067",
                    PostCode = "194044",
                    Country = "643",
                    City = "Новосибирск",
                    Address = "Титова 29/1",
                    BirthDate = "11.06.1988",
                    BirthPlace = "Новосибирск",
                    DocIssueYear = "1999",
                    DocIssueMonth = "07",
                    DocIssueDay = "30",
                    DocIssuedBy = "ТП №20 по СПб и ЛО",
                    OfferAccepted = "1",
                    SmsPhoneNumber = "79612155115"
                }
            };

            return depositionModel;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}