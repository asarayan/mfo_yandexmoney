﻿using System;
using System.Security.Cryptography.X509Certificates;
using YandexMoneyProcessing.Models;
using YandexMoneyProcessing.Services;


namespace testConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var aa = Guid.NewGuid().ToString("N");

            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            Console.WriteLine("--CurrentUser:--");
            foreach (var item in store.Certificates)
            {
                if (item.SerialNumber == "32000045A7EF9CA377E5C8A38E0002000045A7")
                {
                    Console.WriteLine("--------------------ЭТО ОН! (client)--------------------");
                    Console.WriteLine(item.SerialNumber);
                }
                if (item.SerialNumber.Contains("4D5A2566"))
                {
                    Console.WriteLine("--------------------ЭТО ОН! (server)--------------------");
                    Console.WriteLine(item.SerialNumber);
                }
                Console.WriteLine(item.SerialNumber);

            }

            //Console.WriteLine("--LocalMachine:--");
            //X509Store store2 = new X509Store(StoreLocation.LocalMachine);
            //store2.Open(OpenFlags.ReadOnly);

            //foreach (var item in store2.Certificates)
            //{
            //    if (item.SerialNumber == "32000045A7EF9CA377E5C8A38E0002000045A7")
            //        Console.WriteLine("--------------------ЭТО ОН! (client)--------------------");
            //    if (item.SerialNumber == "4D0F5F28")
            //        Console.WriteLine("--------------------ЭТО ОН! (server)--------------------");
            //    Console.WriteLine(item.SerialNumber);
            //}

            //X509Certificate2Collection cers = store.Certificates.Find(X509FindType.FindBySerialNumber, "32000045A7EF9CA377E5C8A38E0002000045A7", false);
            //if (cers.Count > 0)
            //{
            //    cer = cers[0];
            //};
            //store.Close();

            //Console.WriteLine(cers.Count);

            //var orderN = "AzFKCAPbB1P1eu1utXIAnMRXDVsZ.001f.201606";
            //var service = new YandexMoneyService();
            //var errorReason = service.GetErrorReasonBankCard(orderN);
            //var token = service.GetUserTokenBankCard(orderN);

            //Console.WriteLine(errorReason.Amount);
            //Console.WriteLine(errorReason.Reason);
            //Console.WriteLine(token.Usertoken);

            //var deposionModel = GetFakeDepostionModel(orderN);
            //var testDepositionResult = service.TestDepositionToBankCard(deposionModel);
            //Console.WriteLine(testDepositionResult.Status);
            //if (testDepositionResult.Status == "0")
            //{
            // //   var makeDepositionResult = service.MakeDepositionToBankCard(deposionModel);
            // //   Console.WriteLine(makeDepositionResult.Status);
            //}

            //Console.WriteLine("---Autorize---");
            //var autorizeModel = new AuthorizeModel
            //{
            //    ContractId = "777",
            //    Amount = "5.15",
            //    UserToken = token.Usertoken,
            //    Comment = "testComment",
            //    Description = "testDescription",
            //    PaymentParams = new PaymentParams
            //    {
            //        ShopId = "61651",
            //        ShopArticleId = "330970"
            //    }
            //};
            //var autorize = service.AuthorizeFromBankCard(autorizeModel);
            //Console.WriteLine(autorize.Status);
            //Console.WriteLine(autorize.Time);
            //var SystemId = new Guid();
            //bool isYandex = SystemId == new Guid("B5E2B6BA-63DB-47B1-A23A-2F5681026DA1");
            //Console.ReadLine();

            Console.ReadLine();
        }

        private static DepositionModel GetFakeDepostionModel(string orderN)
        {
            Random rnd = new Random();
            var depositionModel = new DepositionModel
            {
                AgentId = "201131",
                DstAccount = "257003392579",
                ClientOrderId = rnd.Next(10, 10000000).ToString(),
                RequestDt = DateTime.Now.ToString("o"),
                Amount = "10.00",
                Currency = "10643",
                Contract = "тест",
                PaymentParamsModel = new PaymentParamsModel
                {
                    SkrOrderN = orderN,
                    FirstName = "Аркадий",
                    MiddleName = "Михайлович",
                    LastName = "Сараян",
                    DocNumber = "4002109067",
                    PostCode = "194044",
                    Country = "643",
                    City = "Новосибирск",
                    Address = "Титова 29/1",
                    BirthDate = "11.06.1988",
                    BirthPlace = "Новосибирск",
                    DocIssueYear = "1999",
                    DocIssueMonth = "07",
                    DocIssueDay = "30",
                    DocIssuedBy = "ТП №20 по СПб и ЛО",
                    OfferAccepted = "1",
                    SmsPhoneNumber = "79612155115"
                }
            };

            return depositionModel;
        }
    }
}
